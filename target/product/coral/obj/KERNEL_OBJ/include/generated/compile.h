/* This file is auto generated, version 6 */
/* SMP PREEMPT */
#define UTS_MACHINE "aarch64"
#define UTS_VERSION "#6 SMP PREEMPT Sun Sep 19 11:35:01 UTC 2021"
#define LINUX_COMPILE_BY "gitpod"
#define LINUX_COMPILE_HOST "ws-d0728d41-0a42-4fd9-9bdd-d619af85be08"
#define LINUX_COMPILER "Android (6052599 based on r353983c1) clang version 9.0.3 (https://android.googlesource.com/toolchain/clang 745b335211bb9eadfa6aa6301f84715cee4b37c5) (https://android.googlesource.com/toolchain/llvm 31c3f8c4ae6cc980405a3b90e7e88db00249eba5) (based on LLVM 9.0.3svn)"
